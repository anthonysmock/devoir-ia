/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontieres;

import java.util.ArrayList;
import jeux.alg.AlgoJeu;
import jeux.alg.Minimax;
import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

/**
 *
 * @author Anthony
 */
public class Test {
    public static void main(String args[])
    {
        Joueur jb = new Joueur("b");
        Joueur jn = new Joueur("n");
        
        PlateauFrontieres pf = new PlateauFrontieres();
        pf.setJoueurs(jb, jn);
        
        pf.joue(jn, PlateauFrontieres.stringToCoup("A1-A2"));
        pf.joue(jn, PlateauFrontieres.stringToCoup("A2-A3"));
        pf.joue(jn, PlateauFrontieres.stringToCoup("A3-A4"));
        pf.joue(jn, PlateauFrontieres.stringToCoup("A4-A5"));
        pf.joue(jn, PlateauFrontieres.stringToCoup("A5-A6"));
        
        pf.joue(jb, PlateauFrontieres.stringToCoup("A8-B7"));
        pf.joue(jb, PlateauFrontieres.stringToCoup("B7-B6"));
        pf.joue(jb, PlateauFrontieres.stringToCoup("B8-C7"));
        pf.joue(jb, PlateauFrontieres.stringToCoup("C8-D7"));
        System.out.println(pf.toString());
        System.out.println(pf.getNbMoiInatteignable(jn));
        
        for(int[] p : pf.getMesPions(jn))
        {
            System.out.println("\n" + p[0] + "\n" + p[1]);
        }
    }
    
}
