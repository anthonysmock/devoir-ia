/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontieres;

import java.util.ArrayList;
import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

/**
 *
 * @author Anthony
 */
public class PlateauFrontieres implements PlateauJeu{

    private static final int BLANC = -1;
    private static final int VIDE = 0;
    private static final int NOIR = 1;
    
    private static Joueur joueurBlanc;
    private static Joueur joueurNoir;
    
    private int[][] plateau = new int[8][8];

    public PlateauFrontieres() {
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
            {
                if(i==0) plateau[i][j] = NOIR;
                else if(i==7) plateau[i][j] = BLANC;
                else plateau[i][j] = VIDE;
            }
    }

    public PlateauFrontieres(int[][] p) {
        
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                this.plateau[i][j] = p[i][j];
    }
    
    @Override
    public boolean coupValide(Joueur j, CoupJeu c) {
        CoupFrontieres cf = (CoupFrontieres)c;
        int ld = cf.getLigneDepart();
        int la = cf.getLigneArrivee();
        int cd = cf.getColonneDepart();
        int ca = cf.getColonneArrivee();
        
        assert ld >=0 && ld <=7;
        assert la >=0 && la <=7;
        assert cd >=0 && cd <=7;
        assert ca >=0 && ca <=7;
        
        if(Math.abs(ca - cd) > 1) return false;
        
        if(j.equals(joueurBlanc))
        {
            if(plateau[ld][cd] != BLANC) return false;
            if(plateau[la][ca] == BLANC) return false;
            if(ld - la != 1) return false;
            return true;
        }
        else if(j.equals(joueurNoir))
        {
            if(plateau[ld][cd] != NOIR) return false;
            if(plateau[la][ca] == NOIR) return false;
            if(la - ld != 1) return false;
            return true;
        }
        else
            return false;
    }

    @Override
    public ArrayList<CoupJeu> coupsPossibles(Joueur joueur) {
        ArrayList<CoupJeu> cp = new ArrayList<>();
        
        int couleurDuJoueur = (isJoueurBlanc(joueur)) ? BLANC : NOIR;
        int incOriente = (couleurDuJoueur==BLANC) ? -1 : 1;
        int max = (incOriente == 1) ? 7 : 8;
        int min = (incOriente == -1) ? 1 : 0;
        for(int i = min; i < max; i++)
            for(int j = 0; j < 8; j++)
            {
                if(plateau[i][j] == couleurDuJoueur)
                {
                    int kmin = (j-1 > 0) ? j-1 : 0;
                    int kmax = (j+1 < 7) ? j+1 : 7;
                    for(int k = kmin; k <= kmax; k++)
                    {
                        if(plateau[i+incOriente][k] != couleurDuJoueur)
                            cp.add(new CoupFrontieres(j,i, k, incOriente+i));
                    }
                }
            }
        return cp;
    }

    @Override
    public boolean finDePartie() {
        
        
        if(coupsPossibles(joueurNoir).isEmpty() && coupsPossibles(joueurBlanc).isEmpty())
            return true;
        
        else return false;
    }

    @Override
    public void joue(Joueur j, CoupJeu c) {
        if(coupValide(j, c))
        {
            CoupFrontieres cf = (CoupFrontieres)c;
            int ld = cf.getLigneDepart();
            int la = cf.getLigneArrivee();
            int cd = cf.getColonneDepart();
            int ca = cf.getColonneArrivee();
        
            plateau[ld][cd] = VIDE;
            plateau[la][ca] = (j.equals(joueurBlanc)) ? BLANC : NOIR;
        }
        
    }

    @Override
    public PlateauJeu copy() {
        return new PlateauFrontieres(this.plateau);
    }
    
    public static void setJoueurs(Joueur jb, Joueur jn) {
		joueurBlanc = jb;
		joueurNoir = jn;
	}

    public boolean isJoueurBlanc(Joueur jb) {
            return joueurBlanc.equals(jb);
    }

    public boolean isJoueurNoir(Joueur jn) {
            return joueurNoir.equals(jn);
    }

    @Override
    public String toString() {
        String s = " ";
        for(int i = 0; i < 8; i++)
            s += "  " + (char)('A'+i) + "  ";
        
        s += "\n";
        
        for(int i = 0; i < 8; i++)
        {
            s += "" + (i+1);
            for(int j = 0; j < 8; j++)
            {
                if(plateau[i][j] == VIDE) s += "[   ]";
                else if(plateau[i][j] == BLANC) s += "[( )]";
                else if(plateau[i][j] == NOIR) s += "[(x)]";
            }
            s += "\n";
        }
        return s;
    }
    
    public int getMonScore(Joueur j)
    {
        int count = 0;
        assert j.equals(joueurBlanc) || j.equals(joueurNoir);
        
        if(j.equals(joueurBlanc))
        {
        for(int i = 0; i < 8; i++)
            if(plateau[0][i] == BLANC) count++;
        }
        else
        {
        for(int i = 0; i < 8; i++)
            if(plateau[7][i] == NOIR) count++;
        }
        
        return count;
    }
    
    public int getSonScore(Joueur j)
    {
        int count = 0;
        assert j.equals(joueurBlanc) || j.equals(joueurNoir);
        
        if(j.equals(joueurNoir))
        {
        for(int i = 0; i < 8; i++)
            if(plateau[0][i] == BLANC) count++;
        }
        else
        {
        for(int i = 0; i < 8; i++)
            if(plateau[7][i] == NOIR) count++;
        }
        
        return count;
    }
    
    public int getNbMesPions(Joueur jo)
    {
        assert jo.equals(joueurBlanc) || jo.equals(joueurNoir);
        int count = 0;
        if(jo.equals(joueurBlanc))
        {
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    if(plateau[i][j] == BLANC) count++;
        }
        else
            for(int i = 0; i < 8; i++)
              for(int j = 0; j < 8; j++)
                  if(plateau[i][j] == NOIR) count++;
        
        return count;
    }
    
    public int getNbSesPions(Joueur jo)
    {
        assert jo.equals(joueurBlanc) || jo.equals(joueurNoir);
        int count = 0;
        if(jo.equals(joueurNoir))
        {
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    if(plateau[i][j] == BLANC) count++;
        }
        else
            for(int i = 0; i < 8; i++)
              for(int j = 0; j < 8; j++)
                  if(plateau[i][j] == NOIR) count++;
        
        return count;
    }
    
    public static CoupFrontieres stringToCoup(String s)
    {
        int id, cd, ia, ca;
        char t[] =  s.toCharArray();
        assert t.length == 5;
        return new CoupFrontieres(t[0], Character.getNumericValue(t[1]), t[3], Character.getNumericValue(t[4]));
    }
    
    /*public int[][] getCpyDamier()
    {
        int[][] res = new int[8][8];
        
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                res[i][j] = this.plateau[i][j];
        
        return res;
    }*/
    
    public int getNbMoiInatteignable(Joueur jo)
    {
        assert jo.equals(joueurBlanc) || jo.equals(joueurNoir);
        int count = 0;
        if(jo.equals(joueurBlanc))
        {
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    if(plateau[i][j] == BLANC)
                    {
                        if(i == 0) count++;
                        else
                        {
                            int m = 1;
                            boolean inatteignable = true;
                            for(int l = i-1; l >= 0; l--)
                            { 
                                int min = (j-m < 0) ? 0 : j - m;
                                int max = (j+m > 7) ? 7 : j + m;
                                for(int c = min; c <= max; c++)
                                {
                                    if(plateau[l][c] == NOIR)
                                    {
                                        inatteignable = false;
                                        break;
                                    }
                                }
                                if(!inatteignable)
                                    break;
                                m++;
                            }
                            if(inatteignable)
                                count++;
                        }
                    }
        }
        else
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    if(plateau[i][j] == NOIR)
                    {
                        if(i == 7) count++;
                        else
                        {
                            int m = 1;
                            boolean inatteignable = true;
                            for(int l = i+1; l < 8; l++)
                            { 
                                int min = (j-m < 0) ? 0 : j - m;
                                int max = (j+m > 7) ? 7 : j + m;
                                for(int c = min; c <= max; c++)
                                {
                                    if(plateau[l][c] == BLANC)
                                    {
                                        inatteignable = false;
                                        break;
                                    }
                                }
                                if(!inatteignable)
                                    break;
                                m++;
                            }
                            if(inatteignable)
                                count++;
                        }
                    }
        
        return count;
    }
    
    public int getNbLuiInatteignable(Joueur jo)
    {
        assert jo.equals(joueurBlanc) || jo.equals(joueurNoir);
        int count = 0;
        if(jo.equals(joueurNoir))
        {
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    if(plateau[i][j] == BLANC)
                    {
                        if(i == 0) count++;
                        else
                        {
                            int m = 1;
                            boolean inatteignable = true;
                            for(int l = i-1; l >= 0; l--)
                            { 
                                int min = (j-m < 0) ? 0 : j - m;
                                int max = (j+m > 7) ? 7 : j + m;
                                for(int c = min; c <= max; c++)
                                {
                                    if(plateau[l][c] == NOIR)
                                    {
                                        inatteignable = false;
                                        break;
                                    }
                                }
                                if(!inatteignable)
                                    break;
                                m++;
                            }
                            if(inatteignable)
                                count++;
                        }
                    }
        }
        else
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                    if(plateau[i][j] == NOIR)
                    {
                        if(i == 7) count++;
                        else
                        {
                            int m = 1;
                            boolean inatteignable = true;
                            for(int l = i+1; l < 8; l++)
                            { 
                                int min = (j-m < 0) ? 0 : j - m;
                                int max = (j+m > 7) ? 7 : j + m;
                                for(int c = min; c <= max; c++)
                                {
                                    if(plateau[l][c] == BLANC)
                                    {
                                        inatteignable = false;
                                        break;
                                    }
                                }
                                if(!inatteignable)
                                    break;
                                m++;
                            }
                            if(inatteignable)
                                count++;
                        }
                    }
        
        return count;
    }
    
    public ArrayList<int[]> getMesPions(Joueur jo)
    {
        ArrayList<int[]> pions = new ArrayList<int[]>();
        assert jo.equals(joueurBlanc) || jo.equals(joueurNoir);
        
        if(jo.equals(joueurBlanc))
        {
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 8; j++)
                {
                    if(plateau[i][j] == BLANC)
                    {
                        int[] tmp = new int[2];
                        tmp[0] = i;
                        tmp[1] = j;
                        pions.add(tmp);
                    }
                }
            }
        }
        else
        {
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 8; j++)
                {
                    if(plateau[i][j] == NOIR)
                    {
                        int[] tmp = new int[2];
                        tmp[0] = i;
                        tmp[1] = j;
                        pions.add(tmp);
                    }
                }
            }
        }
        
        return pions;
    }
    
    public ArrayList<int[]> getSesPions(Joueur jo)
    {
        ArrayList<int[]> pions = new ArrayList<int[]>();
        assert jo.equals(joueurBlanc) || jo.equals(joueurNoir);
        
        if(jo.equals(joueurNoir))
        {
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 8; j++)
                {
                    if(plateau[i][j] == BLANC)
                    {
                        int[] tmp = new int[2];
                        tmp[0] = i;
                        tmp[1] = j;
                        pions.add(tmp);
                    }
                }
            }
        }
        else
        {
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 8; j++)
                {
                    if(plateau[i][j] == NOIR)
                    {
                        int[] tmp = new int[2];
                        tmp[0] = i;
                        tmp[1] = j;
                        pions.add(tmp);
                    }
                }
            }
        }
        
        return pions;
    }
    
    public int[][] getTab()
    {
        int[][] tab = new int[8][8];
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++)
                tab[i][j] = plateau[i][j];
        return tab;
    }
}