/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontieres;

import jeux.alg.AlgoJeu;
import jeux.alg.Minimax;
import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

/**
 *
 * @author Anthony
 */
public class JoueurIA implements IJoueur{
    private int maCouleur;
    private Joueur moi;
    private Joueur lAutre;
    private PlateauFrontieres pf;
    private AlgoJeu alg;
    
    @Override
    public void initJoueur(int mycolour) {
        maCouleur = mycolour;
        pf = new PlateauFrontieres();
        if(maCouleur == BLANC)
        {
            moi = new Joueur("blanc");
            lAutre = new Joueur("noir");
            pf.setJoueurs(moi, lAutre);
        }
        else
        {
            moi = new Joueur("noir");
            lAutre = new Joueur("blanc");
            pf.setJoueurs(lAutre, moi);
        }

        alg = new Minimax(HeuristiquesFrontieres.h, moi, lAutre, 3);
    }

    @Override
    public int getNumJoueur() {
        return maCouleur;
    }

    @Override
    public String choixMouvement() {
        CoupJeu mc = alg.meilleurCoup((pf));
        if(mc == null) return "PASSE";
        pf.joue(moi, mc);
        return ((CoupFrontieres)mc).toString();
    }

    @Override
    public void declareLeVainqueur(int colour) {
        if(maCouleur == colour)
            System.out.println("Yeah !");
        else
            System.out.println("Oh...");
    }

    @Override
    public void mouvementEnnemi(String coup) {
        if(!coup.contentEquals("PASSE"))
            pf.joue(lAutre, PlateauFrontieres.stringToCoup(coup));
    }

    @Override
    public String binoName() {
        return "SMOKE-SOLIMANOUCHE";
    }
    
}
