package frontieres;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anthony
 */
public class JoueurReel implements IJoueur{
    
    int couleurDuJoueur;
    static final String NomDuBinome = "JoueurReel-AS_MS";
    
    public void initJoueur(int mycolour)
    {
        couleurDuJoueur = mycolour;
    }
    
    public int getNumJoueur()
    {
        return couleurDuJoueur;
    }
    public String choixMouvement()
    {
    	String msg = "";
    	String positionInitiale = "", positionFinale = "";
    	System.out.print("Position Initiale: ");
        try{
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            positionInitiale = bufferRead.readLine();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        
    	System.out.print("Position Finale: ");
        try{
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            positionFinale = bufferRead.readLine();        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    	msg = "" + positionInitiale + "-" +positionFinale + "";
        return msg;
    }
    
    public void declareLeVainqueur(int colour)
    {
        if(colour == couleurDuJoueur)
            System.out.println("Yeah c'est gagne !");
        else
            System.out.println("Oh non j'ai perdu, sniff... !");
    }
    
    public void mouvementEnnemi(String coup)
    {
        System.out.println("Coup joue par l'adversaire: " + coup);
    }
    
    public String binoName()
    {
        return NomDuBinome;
    }
}
