/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontieres;

import java.util.ArrayList;
import jeux.alg.Heuristique;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

/**
 *
 * @author Anthony
 */
public class HeuristiquesFrontieres {
    
    public static  Heuristique h = new Heuristique(){ //joue verticalement
				
		public int eval(PlateauJeu p, Joueur j){
                        PlateauFrontieres pf = (PlateauFrontieres)p;
                        int somme = 0;
                        if(pf.finDePartie()){
                            if(pf.getMonScore(j) > pf.getSonScore(j))                                
                            return Integer.MAX_VALUE;
                            else if(pf.getMonScore(j) < pf.getSonScore(j))
                                return Integer.MIN_VALUE;
                            else return 0;
                            }
                        
                        ArrayList<int[]> mesPions = pf.getMesPions(j);
                        ArrayList<int[]> sesPions = pf.getSesPions(j);
                        int[][] plateau = pf.getTab();
                        
                        somme += 4*(pf.getNbMesPions(j) - pf.getNbSesPions(j)); // valeure materielle des pions
                        
                        somme += 64*(pf.getNbMoiInatteignable(j) - pf.getNbLuiInatteignable(j)); //valeur stratégique de pions passes derriere la ligne ennemie
			return somme;
		}
	};
}
