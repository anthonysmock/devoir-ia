package frontieres;


/**
 * Voici l'interface abstraite qu'il suffit d'implanter pour jouer. Ensuite, vous devez utiliser
 * ClientJeu en lui donnant le nom de votre classe pour qu'il la charge et se connecte au serveur.
 * 
 * @author L. Simon (Univ. Paris-Sud)- 2006-2013
 * 
 */

public interface IJoueur {

    // Mais pas lors de la conversation avec l'arbitre (methodes initJoueur et getNumJoueur)
    // Vous pouvez changer cela en interne si vous le souhaitez
    static final int BLANC = -1;
    static final int NOIR = 1;

    /**
     * L'arbitre vient de lancer votre joueur. Il lui informe par cette methode que vous devez jouer
     * dans cette couleur. Vous pouvez utiliser cette methode abstraite, ou la methode constructeur
     * de votre classe, pour initialiser vos structures.
     * 
     * @param mycolour
     *            La couleur dans laquelle vous allez jouer (-1=BLANC, 1=NOIR)
     */
    public void initJoueur(int mycolour);

    // Doit retourner l'argument pase� par la fonction ci-dessus (constantes BLANC ou NOIR)
    public int getNumJoueur();

    /**
     * C'est ici que vous devez faire appel a votre IA pour trouver le meilleur coup a jouer sur le
     * plateau courant.
     * 
     * @return une chaine decrivant le mouvement. Cette chaine doit etre decrite exactement comme
     *         sur l'exemple : String msg = "" + positionInitiale + "-" +positionFinale + ""; ou "PASSE";
     *          Chaque position contient une lettre et un numero, par exemple:A1,B2 (coup "A1-B2")
     */
    public String choixMouvement();
    

    /**
     * Methode appelee par l'arbitre pour designer le vainqueur. Vous pouvez en profiter pour
     * imprimer une banni?re de joie... Si vous gagnez...
     * 
     * @param colour
     *            La couleur du gagnant (BLANC=-1, NOIR=1).
     */
    public void declareLeVainqueur(int colour);

    /**
     * On suppose que l'arbitre a verifie que le mouvement ennemi etait bien legal. Il vous informe
     * du mouvement ennemi. A vous de repercuter ce mouvement dans vos structures. Comme par exemple
     * eliminer les pions que ennemi vient de vous prendre par ce mouvement. Il n'est pas necessaire
     * de reflchir deja a votre prochain coup a jouer : pour cela l'arbitre appelera ensuite
     * choixMouvement().
     * 
     * @param coup
     * 			une chaine decrivant le mouvement:  par exemple: "A1-B2" ou "PASSE"
     */
    public void mouvementEnnemi(String coup);

    public String binoName();

}
