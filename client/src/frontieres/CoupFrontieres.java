/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontieres;
import jeux.modele.CoupJeu;
/**
 *
 * @author Anthony
 */
public class CoupFrontieres implements CoupJeu{
    private int colonneDepart;
    private int ligneDepart;
    private int colonneArrivee;
    private int ligneArrivee;

    public CoupFrontieres(int colonned ,int ligned, int colonnea, int lignea) {
        this.colonneDepart = colonned;
        this.ligneDepart = ligned;
        this.colonneArrivee = colonnea;
        this.ligneArrivee = lignea;
        /*assert this.ligneDepart >= 0;
        assert this.ligneDepart <= 7;
        assert this.colonneArrivee >= 0;
        assert this.colonneDepart <= 7;*/
    }
    
    public CoupFrontieres(char colonned, int ligned, char colonnea, int lignea) {
        this.colonneDepart = colonned-'A';
        this.ligneDepart = ligned-1;
        this.colonneArrivee = colonnea-'A';
        this.ligneArrivee = lignea-1;
        /*assert this.ligneDepart >= 0;
        assert this.ligneDepart <= 7;
        assert this.colonneArrivee >= 0;
        assert this.colonneDepart <= 7;*/
    }

    public int getColonneDepart() {
        return colonneDepart;
    }

    public int getLigneDepart() {
        return ligneDepart;
    }

    public int getLigneArrivee() {
        return ligneArrivee;
    }

    public int getColonneArrivee() {
        return colonneArrivee;
    }
    
    @Override
    public String toString() {
        return "" + (char)(colonneDepart+'A') + (ligneDepart+1) + "-" + (char)(colonneArrivee+'A') + (ligneArrivee+1);
    }

}
