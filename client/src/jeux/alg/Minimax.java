/**
 * 
 */

package jeux.alg;

import java.util.ArrayList;

import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

public class Minimax implements AlgoJeu {

    /** La profondeur de recherche par défaut
     */
    private final static int PROFMAXDEFAUT = 4;

   
    // -------------------------------------------
    // Attributs
    // -------------------------------------------
 
    /**  La profondeur de recherche utilisée pour l'algorithme
     */
    private int profMax = PROFMAXDEFAUT;

     /**  L'heuristique utilisée par l'algorithme
      */
   private Heuristique h;

    /** Le joueur Min
     *  (l'adversaire) */
    private Joueur joueurMin;

    /** Le joueur Max
     * (celui dont l'algorithme de recherche adopte le point de vue) */
    private Joueur joueurMax;

    /**  Le nombre de noeuds développé par l'algorithme
     * (intéressant pour se faire une idée du nombre de noeuds développés) */
       private int nbnoeuds;

    /** Le nombre de feuilles évaluées par l'algorithme
     */
    private int nbfeuilles;


  // -------------------------------------------
  // Constructeurs
  // -------------------------------------------
    public Minimax(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
        this(h,joueurMax,joueurMin,PROFMAXDEFAUT);
    }

    public Minimax(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
        this.h = h;
        this.joueurMin = joueurMin;
        this.joueurMax = joueurMax;
        profMax = profMaxi;
//		System.out.println("Initialisation d'un MiniMax de profondeur " + profMax);
    }

   // -------------------------------------------
  // Méthodes de l'interface AlgoJeu
  // -------------------------------------------
   public CoupJeu meilleurCoup(PlateauJeu p) {
        ArrayList<CoupJeu> cp = p.coupsPossibles(joueurMax);
        CoupJeu meilleurCoup = null;
        int prof = 2;
        int valMax = Integer.MIN_VALUE;
        int val;
        if(!cp.isEmpty())
        {
            meilleurCoup = cp.get(0);
            valMax = minimax(p.copy(), cp.get(0), prof);
            val = valMax;
        }
        
        if(cp.size() >= 2)
            for(CoupJeu c : cp.subList(1, cp.size()))
            {
                val = minimax(p.copy(), c, prof);
                if(val > valMax)
                {
                    meilleurCoup = c;
                    valMax = val;
                }
            }
        return meilleurCoup;

    }
  // -------------------------------------------
  // Méthodes publiques
  // -------------------------------------------
    public String toString() {
        return "MiniMax(ProfMax="+profMax+")";
    }



  // -------------------------------------------
  // Méthodes internes
  // -------------------------------------------

    //A vous de jouer pour implanter Minimax
    
    private int minimax(PlateauJeu p, CoupJeu c, int profActuelle)
    {
        boolean pair = profActuelle % 2 == 0;
        
        Joueur joueurQuiAJoue = (pair) ? joueurMax : joueurMin;
        Joueur joueurQuiVaJouer = (pair) ? joueurMin : joueurMax;
        //System.out.println("(prof : " + profActuelle + " ) " + joueurActuel.toString() + " veut joueur " + c.toString() + " sur :\n" + p.toString());
        p.joue(joueurQuiAJoue, c);
        ArrayList<CoupJeu> cp = p.coupsPossibles(joueurQuiVaJouer);
        
        if(profActuelle < profMax && !cp.isEmpty())
        {   
            nbnoeuds++;
            int minMax, val;
            minMax = minimax(p.copy(), cp.get(0), profActuelle + 1);
            if(cp.size() >= 2)
                for(CoupJeu cs : cp.subList(1, cp.size()))
                {
                    val = minimax(p.copy(), cs, profActuelle + 1);
                    if(pair)
                        minMax = (val < minMax) ? val : minMax;
                    else
                        minMax = (val > minMax) ? minMax : val;
                }
            
            return minMax;
        }
        else
        {
            nbfeuilles++;
            return h.eval(p, joueurQuiAJoue);
        }
    }

}
