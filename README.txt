Pour tester si le .jar marche, ce placer dans le dossier serveur puis lancer les commandes suivantes (dans des terminaux differents) :

1. java -cp obffrontieres.jar frontieres.ServeurJeu 1234 1
2. java -cp obffrontieres.jar frontieres.ClientJeu frontieres.JoueurAleatoire localhost 1234
3. java -cp obffrontieres.jar frontieres.ClientJeu frontieres.JoueurAleatoire localhost 1234 #oui, c'est la meme pour que deux joueurs jouen ensemble.

Pour utiliser nos classes :
1. Les compiler :
	*cd frontieres
	*javac Applet.java
	*#javac IJoueur.java
	*javac ClientJeu.java IJoueur.java
	*javac JoueurReel.java IJoueur.java

Puis les utiliser :
2.
	*java -cp . frontieres.ClientJeu frontieres.JoueurReel localhost 1234 #x2